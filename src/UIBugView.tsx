import * as React from "react";
import { View, StyleSheet, Alert, FlatList, Dimensions } from "react-native";
import { Event, EventType } from "./types";
import { ViewStyle } from "react-native";
import { Text } from "react-native";
import { TextStyle } from "react-native";
import { useState } from "react";
import { ScrollView } from "react-native";
import { TouchableOpacity } from "react-native";
import { Clipboard } from "react-native";
//@ts-ignore
import JSONTree from 'react-native-json-tree'
import { safeStringify } from "./utils";

const screen = Dimensions.get("window");

const UIBugView: React.FC<{
    subscribeForceUpdate: (a: (timeline: Event) => void) => () => void,
    onClose: () => void,
    onSendLog: (path?: string)=>void,
    onSendHistory: ()=>void
}> = ({ subscribeForceUpdate, onClose, onSendLog, onSendHistory }) => {
    const [viewEvent, setViewEvent] = useState<Event | undefined>();
    const [eventText, setTextEvent] = useState<string>('wait...');
    const [minimize, setMiniState] = useState(false);
    const [timeline, setTimeline] = useState<Event[]>([]);
    const [filterEvent, setFilter] = useState<EventType | undefined>();

    const unsubscribe = React.useRef(() => { })

    React.useEffect(() => {
        unsubscribe.current = subscribeForceUpdate((event) => {
            setTimeline(events => [...events, event])
        });
        return () => {
            unsubscribe.current();
        }
    }, [])

    const formattedData = (time: number): string => {
        const date = new Date(time);
        return `${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}.${date.getMilliseconds()}`;
    }

    const renderBody = (type: EventType, data: any[]): string => {
        if (type === "request") {
            return `${data[0].method} ${data[0].url}`
        }
        if (type === "response") {
            return `${data[0].method} ${data[0].url}  (${data[0].status})`
        }
        return `${data.map(item=>safeStringify(item)).join(" ")}`
    }

    const colorizeType = (type: EventType) => {
        const defaultColor = "white";

        const map: Record<EventType, string> = {
            [EventType.debug]: defaultColor,
            [EventType.info]: "cyan",
            [EventType.warn]: "orange",
            [EventType.error]: "red",
            [EventType.navigate]: defaultColor,
            [EventType.response]: "rgb(10, 120, 255)",
            [EventType.request]: "rgb(255, 255, 150)",
            [EventType.fatal]: defaultColor
        }

        return <Text style={{ color: map[type] || defaultColor }}>{` [${type}]  `}</Text>
    }

    const getEventTypes = (): Array<EventType> => {
        const res: EventType[] = []
        for (const eventName in EventType) {
            res.push(eventName as EventType)
        }
        return res
    }

    const sendLogs = React.useCallback(()=>{
        Alert.alert(
            "Внимание",
            "Выберете действие",
            [
                { text: "Текущая сессиия", onPress: onSendLog},
                { text: "Прошлые сессиии", onPress: onSendHistory},
                { text: "Отмена", onPress: ()=>{}, style: "destructive" }
            ]
        )
    },[])

    return (
        <View style={[styles.root, !!viewEvent && { height: screen.height - 100 }, minimize && { height: "auto" },]}>
            <View style={styles.header}>
                <Text style={[{ flex: 1 }, styles.text]}>BugView</Text>
                <Text style={[styles.text, { marginHorizontal: 10 }]} onPress={()=>setTimeline([])}>[clear]</Text>
                <Text style={[styles.text, { marginHorizontal: 10 }]} onPress={sendLogs}>[send logs]</Text>
                <Text style={[styles.text, { marginHorizontal: 10 }]} onPress={() => setMiniState(s => !s)}>[minimize]</Text>
                <Text style={[styles.text, { marginLeft: 10 }]} onPress={onClose}>Х</Text>
            </View>
            {
                viewEvent &&
                !minimize &&
                <ScrollView contentContainerStyle={{ paddingHorizontal: 20 }}>
                    <View style={[{ marginBottom: 10, flexDirection: "row" }]}>
                        <Text style={[styles.text, { flex: 1 }]} onPress={() => setViewEvent(undefined)}>{"<-- Back"}</Text>
                        <Text style={[styles.text]} onPress={() => Clipboard.setString(eventText)}>{"[copy]"}</Text>
                    </View>
                    <JSONTree data={viewEvent} />

                </ScrollView>

            }
            {
                !viewEvent &&
                !minimize &&
                <ScrollView contentContainerStyle={{ paddingHorizontal: 20, height: 40 }} showsHorizontalScrollIndicator={false} horizontal>
                    {
                        getEventTypes().map((eventName) => (
                            <Text
                                key={eventName}
                                style={[
                                    styles.text,
                                    { marginRight: 20, marginVertical: 5 },
                                    (!!filterEvent && filterEvent !== eventName) && { opacity: .5 }
                                ]}
                                onPress={() => setFilter(e => { if (e === eventName) return undefined; return eventName })}
                            >{colorizeType(eventName)}</Text>
                        ))
                    }
                </ScrollView>
            }
            {
                !viewEvent &&
                !minimize &&
                <FlatList
                    data={timeline.filter(e => e.type === filterEvent || !filterEvent).reverse()}
                    keyExtractor={(item) => item.uuid}
                    inverted
                    renderItem={({ item }) => {
                        const { time, type, data } = item;
                        return (
                            <TouchableOpacity
                                onPress={() => {
                                    setViewEvent(item);
                                    setTextEvent('wait...');
                                    setTimeout(() => {
                                        setTextEvent(safeStringify(item))
                                    }, 100)

                                }}
                                style={{ flexDirection: "row", paddingHorizontal: 20, marginVertical: 2 }}
                            >
                                <Text style={[{ flex: 1 }, styles.text]} numberOfLines={2}>
                                    {`${formattedData(time)}`}
                                    {colorizeType(type)}
                                    {renderBody(type, data)}
                                </Text>
                                <Text style={styles.text}>
                                    {"-->"}
                                </Text>
                            </TouchableOpacity>
                        )
                    }}
                    ItemSeparatorComponent={() => <View style={styles.separator} />}
                />
            }

        </View>
    )
}


const styles = StyleSheet.create<{
    header: ViewStyle,
    root: ViewStyle,
    text: TextStyle,
    separator: ViewStyle
}>({
    header: {
        flexDirection: "row",
        paddingHorizontal: 20,
        paddingVertical: 5,
        backgroundColor: "rgba(0,0,0,.3)"
    },
    root: {
        position: "absolute",
        bottom: 0,
        left: 0,
        right: 0,
        height: 340,
        backgroundColor: "rgba(0,0,0,.7)"
    },
    text: {
        color: "white"
    },
    separator: {
        height: StyleSheet.hairlineWidth,
        width: "100%",
        backgroundColor: "white",
        opacity: .4
    }
})

export default UIBugView;
