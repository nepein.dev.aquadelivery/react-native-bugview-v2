import * as React from "react";

export type TBugVIewContext = {
    navigationEvent: (screen: string, params?: any) => void,
    showUI: (state: boolean) => void,
    showingUI: boolean,
    bugviewVersion: string
}
//@ts-ignore
export default React.createContext<TBugVIewContext>({})