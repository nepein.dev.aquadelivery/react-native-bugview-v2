export function safeStringify(json: object) {
    try {
        let cache: any[] | null = [];
        const result = JSON.stringify(json, (key, value) => {
            if (typeof value === "function") {
                return "[Function]"
            }

            if (typeof value === 'object' && value !== null && Array.isArray(cache)) {
                // Duplicate reference found, discard key
                //@ts-ignore
                if (cache.includes(value)) return "[Cylcular struct]";
                // Store value in our collection
                cache.push(value);
            }
            return value;
        });
        cache = null;
        return result;
    } catch (e) {
        console.log("error@", e);
        return `bugview: Could not serialize data`
    }



}