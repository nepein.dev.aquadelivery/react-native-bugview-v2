var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
import * as React from "react";
import { View, StyleSheet, Alert, FlatList, Dimensions } from "react-native";
import { EventType } from "./types";
import { Text } from "react-native";
import { useState } from "react";
import { ScrollView } from "react-native";
import { TouchableOpacity } from "react-native";
import { Clipboard } from "react-native";
//@ts-ignore
import JSONTree from 'react-native-json-tree';
import { safeStringify } from "./utils";
var screen = Dimensions.get("window");
var UIBugView = function (_a) {
    var subscribeForceUpdate = _a.subscribeForceUpdate, onClose = _a.onClose, onSendLog = _a.onSendLog, onSendHistory = _a.onSendHistory;
    var _b = useState(), viewEvent = _b[0], setViewEvent = _b[1];
    var _c = useState('wait...'), eventText = _c[0], setTextEvent = _c[1];
    var _d = useState(false), minimize = _d[0], setMiniState = _d[1];
    var _e = useState([]), timeline = _e[0], setTimeline = _e[1];
    var _f = useState(), filterEvent = _f[0], setFilter = _f[1];
    var unsubscribe = React.useRef(function () { });
    React.useEffect(function () {
        unsubscribe.current = subscribeForceUpdate(function (event) {
            setTimeline(function (events) { return __spreadArrays(events, [event]); });
        });
        return function () {
            unsubscribe.current();
        };
    }, []);
    var formattedData = function (time) {
        var date = new Date(time);
        return date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds() + "." + date.getMilliseconds();
    };
    var renderBody = function (type, data) {
        if (type === "request") {
            return data[0].method + " " + data[0].url;
        }
        if (type === "response") {
            return data[0].method + " " + data[0].url + "  (" + data[0].status + ")";
        }
        return "" + data.map(function (item) { return safeStringify(item); }).join(" ");
    };
    var colorizeType = function (type) {
        var _a;
        var defaultColor = "white";
        var map = (_a = {},
            _a[EventType.debug] = defaultColor,
            _a[EventType.info] = "cyan",
            _a[EventType.warn] = "orange",
            _a[EventType.error] = "red",
            _a[EventType.navigate] = defaultColor,
            _a[EventType.response] = "rgb(10, 120, 255)",
            _a[EventType.request] = "rgb(255, 255, 150)",
            _a[EventType.fatal] = defaultColor,
            _a);
        return React.createElement(Text, { style: { color: map[type] || defaultColor } }, " [" + type + "]  ");
    };
    var getEventTypes = function () {
        var res = [];
        for (var eventName in EventType) {
            res.push(eventName);
        }
        return res;
    };
    var sendLogs = React.useCallback(function () {
        Alert.alert("Внимание", "Выберете действие", [
            { text: "Текущая сессиия", onPress: onSendLog },
            { text: "Прошлые сессиии", onPress: onSendHistory },
            { text: "Отмена", onPress: function () { }, style: "destructive" }
        ]);
    }, []);
    return (React.createElement(View, { style: [styles.root, !!viewEvent && { height: screen.height - 100 }, minimize && { height: "auto" },] },
        React.createElement(View, { style: styles.header },
            React.createElement(Text, { style: [{ flex: 1 }, styles.text] }, "BugView"),
            React.createElement(Text, { style: [styles.text, { marginHorizontal: 10 }], onPress: function () { return setTimeline([]); } }, "[clear]"),
            React.createElement(Text, { style: [styles.text, { marginHorizontal: 10 }], onPress: sendLogs }, "[send logs]"),
            React.createElement(Text, { style: [styles.text, { marginHorizontal: 10 }], onPress: function () { return setMiniState(function (s) { return !s; }); } }, "[minimize]"),
            React.createElement(Text, { style: [styles.text, { marginLeft: 10 }], onPress: onClose }, "\u0425")),
        viewEvent &&
            !minimize &&
            React.createElement(ScrollView, { contentContainerStyle: { paddingHorizontal: 20 } },
                React.createElement(View, { style: [{ marginBottom: 10, flexDirection: "row" }] },
                    React.createElement(Text, { style: [styles.text, { flex: 1 }], onPress: function () { return setViewEvent(undefined); } }, "<-- Back"),
                    React.createElement(Text, { style: [styles.text], onPress: function () { return Clipboard.setString(eventText); } }, "[copy]")),
                React.createElement(JSONTree, { data: viewEvent })),
        !viewEvent &&
            !minimize &&
            React.createElement(ScrollView, { contentContainerStyle: { paddingHorizontal: 20, height: 40 }, showsHorizontalScrollIndicator: false, horizontal: true }, getEventTypes().map(function (eventName) { return (React.createElement(Text, { key: eventName, style: [
                    styles.text,
                    { marginRight: 20, marginVertical: 5 },
                    (!!filterEvent && filterEvent !== eventName) && { opacity: .5 }
                ], onPress: function () { return setFilter(function (e) { if (e === eventName)
                    return undefined; return eventName; }); } }, colorizeType(eventName))); })),
        !viewEvent &&
            !minimize &&
            React.createElement(FlatList, { data: timeline.filter(function (e) { return e.type === filterEvent || !filterEvent; }).reverse(), keyExtractor: function (item) { return item.uuid; }, inverted: true, renderItem: function (_a) {
                    var item = _a.item;
                    var time = item.time, type = item.type, data = item.data;
                    return (React.createElement(TouchableOpacity, { onPress: function () {
                            setViewEvent(item);
                            setTextEvent('wait...');
                            setTimeout(function () {
                                setTextEvent(safeStringify(item));
                            }, 100);
                        }, style: { flexDirection: "row", paddingHorizontal: 20, marginVertical: 2 } },
                        React.createElement(Text, { style: [{ flex: 1 }, styles.text], numberOfLines: 2 }, "" + formattedData(time),
                            colorizeType(type),
                            renderBody(type, data)),
                        React.createElement(Text, { style: styles.text }, "-->")));
                }, ItemSeparatorComponent: function () { return React.createElement(View, { style: styles.separator }); } })));
};
var styles = StyleSheet.create({
    header: {
        flexDirection: "row",
        paddingHorizontal: 20,
        paddingVertical: 5,
        backgroundColor: "rgba(0,0,0,.3)"
    },
    root: {
        position: "absolute",
        bottom: 0,
        left: 0,
        right: 0,
        height: 340,
        backgroundColor: "rgba(0,0,0,.7)"
    },
    text: {
        color: "white"
    },
    separator: {
        height: StyleSheet.hairlineWidth,
        width: "100%",
        backgroundColor: "white",
        opacity: .4
    }
});
export default UIBugView;
