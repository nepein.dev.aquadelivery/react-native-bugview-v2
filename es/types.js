export var EventType;
(function (EventType) {
    EventType["debug"] = "debug";
    EventType["info"] = "info";
    EventType["warn"] = "warn";
    EventType["error"] = "error";
    EventType["request"] = "request";
    EventType["response"] = "response";
    EventType["navigate"] = "navigate";
    EventType["fatal"] = "fatal";
})(EventType || (EventType = {}));
